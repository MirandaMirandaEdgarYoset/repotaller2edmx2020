function getProductos(){
  let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  let request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(this.readyState==4&&this.status==200){
      //console.table(JSON.parse(request.responseText).value);
      procesarProductos(request.responseText)
    }
  }
  request.open("GET",url,true);
  request.send();
}

let procesarProductos = (products) =>{
  let JSONProductos=JSON.parse(products);
  //alert(JSONProductos.value[0].ProductName);
  let divTabla = document.getElementById("divTablaProductos");
  let tabla = document.createElement("table");
  let tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  for (var i = 0; i < JSONProductos.value.length; i++) {
    let nuevaFila = document.createElement("tr");
    let columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;

    let columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

    let columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla)
}
