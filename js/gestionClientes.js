function getClientes(){
  let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";//?$filter=Country eq 'Germany'";
  let request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(this.readyState==4&&this.status==200){
      //console.table(JSON.parse(request.responseText).value);
      procesarClientes(request.responseText);

    }
  }
  request.open("GET",url,true);
  request.send();
}

let procesarClientes = (clientes) =>{
  let JSONClientes=JSON.parse(clientes);

  let divTabla = document.getElementById("divTablaClientes");
  let tabla = document.createElement("table");
  let tbody = document.createElement("tbody");
  let urlFlag= "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  for (var i = 0; i < JSONClientes.value.length; i++) {
    let nuevaFila = document.createElement("tr");
    let columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    let columnaCity = document.createElement("td");
    columnaCity.innerText = JSONClientes.value[i].City;

    let columnaFlag = document.createElement("td");
    let imgFlag = document.createElement("img");
    imgFlag.src= urlFlag+JSONClientes.value[i].Country+".png";
    imgFlag.classList.add("flag")
    if(JSONClientes.value[i].Country=="UK"){
    imgFlag.src= urlFlag+"United-Kingdom.png";
    }
    else{
      imgFlag.src= urlFlag+JSONClientes.value[i].Country+".png";
    }

    columnaFlag.appendChild(imgFlag);
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaFlag);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla)
}
